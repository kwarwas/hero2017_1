// https://kwarwas@bitbucket.org/kwarwas/hero2017_1.git

#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class RandomNumbers
{
public:
	static void randomize()
	{
		srand(time(nullptr));
		rand();
	}

	static int random(int min, int max)
	{
		return (double)rand() / RAND_MAX * (max - min + 1) + min;
	}
};

class Przedmiot
{
};

class Plecak
{
	int pojemnosc = 10;
	Przedmiot *przedmioty;

public:
	Plecak()
	{
		przedmioty = new Przedmiot[pojemnosc];
	}
};

class Postac
{
	Plecak plecak;
	
	string nazwa;
	int sila;
	int zrecznosc;
	int wytrzymalosc;

public:
	Postac(string nazwa, 
		int sila = 6, int zrecznosc = 2, int wytrzymalosc = 20)
		: nazwa(nazwa),
		sila(sila), 
		zrecznosc(zrecznosc), 
		wytrzymalosc(wytrzymalosc)
	{
	}

	void walka(Postac &other)
	{
		cout << "Rozpoczynasz walke z " << other.nazwa << endl;

		int runda = 1;
		do
		{
			cout << "Runda " << runda++ << endl;

			double sa1 = sila + 0.5 * zrecznosc + RandomNumbers::random(1, 6);
			double sa2 = other.sila + 0.5 * other.zrecznosc + RandomNumbers::random(1, 6);

			cout << "Sila ataku: " << nazwa << " " << sa1 << endl
				<< "Sila ataku: " << other.nazwa << " " << sa2 << endl;

			if (sa1 > sa2)
			{
				other.wytrzymalosc -= sa1 - sa2;
			}
			else
			{
				wytrzymalosc -= sa2 - sa1;
			}

			drukStats();
			other.drukStats();
		} 
		while (wytrzymalosc > 0 && other.wytrzymalosc > 0);

		if (wytrzymalosc <= 0)
		{
			cout << "GAME OVER dla " << nazwa << endl;
			cout << "Wygral " << other.nazwa << endl;
		}
		else
		{
			cout << "GAME OVER dla " << other.nazwa << endl;
			cout << "Wygral " << nazwa << endl;
		}
	}

	void drukStats() const
	{
		cout << nazwa << endl
			<< "Sila: " << sila << endl
			<< "Zrecznosc: " << zrecznosc << endl
			<< "Wytrzymalosc: " << wytrzymalosc << endl << endl;
	}
};

int main()
{
	RandomNumbers::randomize();

	Postac janek
	(
		"janek",
		RandomNumbers::random(5, 10),
		RandomNumbers::random(2, 7),
		RandomNumbers::random(15, 35)
	);

	Postac karol
	(
		"karol",
		RandomNumbers::random(3, 8),
		RandomNumbers::random(5, 10),
		RandomNumbers::random(15, 35)
	);

	janek.walka(karol);
}
